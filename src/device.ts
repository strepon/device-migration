export interface Device {
    code: string;
    // device migration status, one of "initial", "changing_device", "changing_infosystem",
    // "done", "error_changing_device" and "error_changing_infosystem"
    // this status is saved to local storage and used when the storage is loaded and applied
    status: string;
    // detailed status used only to show description of the migration process
    // difference from status: "changing_device" -> "changing_device_waiting",
    // "changing_device_processing", "changing_device_done"
    // "changing_infosystem" -> "changing_infosystem_processing", "changing_infosystem_done"
    statusDetailed: string;
    targetUrl: string;
    originalUrls: string[];
}
