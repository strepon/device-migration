import {I18N} from "aurelia-i18n";
import {inject} from "aurelia-framework";
import {bindable} from "aurelia-framework";
import {observable} from "aurelia-framework";
import jsPDF from "jspdf";
import "jspdf-autotable";
import "@material/mwc-fab";
import "@material/mwc-textfield";
import "@material/mwc-list/mwc-list";
import "@material/mwc-list/mwc-list-item";
import "@material/mwc-icon-button";
import "@material/mwc-dialog";
import "@material/mwc-button";
import "@material/mwc-select";
import {Device} from "./device";
import {ubuntuFont, ubuntuBoldFont} from "./resources/ubuntu-font";

interface Report {
    name: string;
    createdTime: string;
    devices: Device[];
    type: string; // one of "all", "failed", "successful"
}

@inject(I18N)
export class ReportsCustomElement {
    @bindable devices: Device[];
    reports: Report[] = [];
    selectedReport: Report;
    i18n: I18N;
    addReportDialog: any;
    textReportName: any;
    deleteReportDialog: any;
    newReportType = "all";
    @observable newReportName = "";
    dialogValidInputs = false;

    constructor(i18n: I18N) {
        this.i18n = i18n;
        this.i18n.setLocale("cs");

        const storedReports = window.localStorage.getItem("deviceMigrationReports");
        if (storedReports) {
            this.reports = JSON.parse(storedReports);
        }
    }

    newReportNameChanged(): void {
        if (this.textReportName) {
            this.dialogValidInputs = this.textReportName.checkValidity();
        }
    }

    /**
     * Shows the dialog with settings for a new report.
     */
    openAddReportDialog(): void {
        this.addReportDialog.open = true;
    }

    addReportDialogClosed(e): void {
        if (e.detail && e.detail.action === "create") {
            this.createNewReport();
        }
    }

    /**
     * Creates a new report from settings entered into the dialog
     * and from the current list of devices (makes a snapshot of them).
     */
    createNewReport(): void {
        const dateTime: Date = new Date();
        const dateTimeFormatted = dateTime.toISOString().slice(0, 10) + " " + dateTime.toISOString().slice(11, 19);
        const devicesToReport: Device[] = [];
        this.devices.forEach(function(device): void {
            const isFailed = device.status.includes("error");
            const isSuccessful = device.status === "done";
            if (this.newReportType === "all" || (this.newReportType === "failed" && isFailed) ||
                (this.newReportType === "successful" && isSuccessful)) {
                devicesToReport.push(device);
            }
        }, this);
        this.reports.push({
            name: this.newReportName, type: this.newReportType, createdTime: dateTimeFormatted,
            devices: JSON.parse(JSON.stringify(devicesToReport))
        });
        this.reportsChanged();
    }

    reportsChanged(): void {
        window.localStorage.setItem("deviceMigrationReports", JSON.stringify(this.reports));
    }

    deleteReportClicked(report: Report): void {
        this.deleteReportDialog.open = true;
        this.selectedReport = report;
    }

    deleteReportDialogClosed(e): void {
        if (e.detail.action === "delete") {
            this.reports.splice(this.reports.indexOf(this.selectedReport), 1);
            this.reportsChanged();
        }
    }

    downloadReport(report: Report): void {
        const devicesToExport = [];
        report.devices.forEach(function(device): void {
            devicesToExport.push([
                device.code, this.i18n.tr(device.status), device.originalUrls ? device.originalUrls : "",
                device.targetUrl ? device.targetUrl : ""
            ]);
        }, this);

        const doc = new jsPDF("landscape");
        doc.addFileToVFS("Ubuntu.normal.ttf", ubuntuFont);
        doc.addFileToVFS("Ubuntu.bold.ttf", ubuntuBoldFont);
        doc.addFont("Ubuntu.normal.ttf", "Ubuntu", "normal");
        doc.addFont("Ubuntu.bold.ttf", "Ubuntu", "bold");
        doc.setFont("Ubuntu");

        // report header
        doc.setFontSize(24);
        doc.text(report.name, 15, 20);
        doc.setFontSize(16);
        doc.text(this.i18n.tr("Reports." + report.type), 15, 33);
        doc.setFontSize(10);
        doc.text(this.i18n.tr("Reports.Created") + " " + report.createdTime, 15, 40);

        // table of devices
        doc.autoTable({
            head: [[
                this.i18n.tr("Reports.DeviceCode"), this.i18n.tr("Reports.MigrationStatus"),
                this.i18n.tr("Reports.OriginalUrls"), this.i18n.tr("Reports.TargetUrl")
            ]],
            styles: { font: "Ubuntu" },
            body: devicesToExport,
            startY: 50
        });
        
        doc.save(this.i18n.tr("Reports.report") + "_" + report.createdTime.replace(" ", "_") + ".pdf");
    }
}
