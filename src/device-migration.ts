import {HttpClient} from "aurelia-fetch-client";
import {I18N} from "aurelia-i18n";
import {inject} from "aurelia-framework";
import {observable} from "aurelia-framework";
import {bindable} from "aurelia-framework";
import "device-migration.css";
import "@material/mwc-fab";
import "@material/mwc-list/mwc-list";
import "@material/mwc-list/mwc-list-item";
import "@material/mwc-menu";
import "@material/mwc-icon";
import "@material/mwc-icon-button";
import "@material/mwc-dialog";
import "@material/mwc-button";
import "@material/mwc-select";
import "@material/mwc-top-app-bar-fixed";
import "@material/mwc-linear-progress";
import {Device} from "./device";

interface Server {
    url: string;
    name: string;
}

@inject(I18N)
export class DeviceMigration {
    webApiUrl = "http://localhost:8082/";
    devices: Device[] = [];
    selectedDevice: Device;
    targetServers: Server[] = [];
    mode = "showingDevices";
    iconsForStatus = {
        initial: "remove", changing: "fast_forward", error: "cancel", done: "done"
    };
    @bindable @observable scannedDeviceCode = "";
    selectedTargetServerUrl = "";
    anyDeviceInProgress = false;
    i18n: I18N;
    selectTargetElem: any;
    migrateDialog: any;
    deleteAllDialog: any;
    deleteDeviceDialog: any;
    topBar: any;
    appMenu: any;

    constructor(i18n: I18N) {
        this.i18n = i18n;
        this.i18n.setLocale("cs");

        // adjust Web API URL based on frontend URL
        const originSplitted = window.location.origin.split(":");
        if (originSplitted.length > 1) {
            this.webApiUrl = [originSplitted[0], originSplitted[1], "8082"].join(":") + "/";
        }

        const storedDevices = window.localStorage.getItem("deviceMigrationDevices");
        if (storedDevices) {
            this.devices = JSON.parse(storedDevices);
            this.devices.forEach(function(device) {
                device.statusDetailed = device.status;
            });
        }

        // device status is ongoing change -> get current state from backend
        this.devices.forEach(function(device): void {
            if (device.status.includes("changing")) {
                this.checkMigrationStatus(device);
            }
        }, this);

        // load available target servers
        this.doFetch("targetservers")
        .then(function(servers): void {
            servers.sort(function(server1, server2) {
                return server1.name.localeCompare(server2.name);
            });
            this.targetServers = servers;
        }.bind(this))
        .catch(function(error) {
            console.error(error);
        });
    }

    attached(): void {
        // fix strange padding for the top bar
        const topBarHeaderStyle = this.topBar.shadowRoot.querySelector("header").style;
        topBarHeaderStyle.top = topBarHeaderStyle.right = "0px";
    }

    scannedDeviceCodeChanged(newCode: string): void {
        if (newCode) {
            this.addDevice(newCode);
            // to not block repeated addition of the same code (even if it does not make too much sense)
            this.scannedDeviceCode = "";
        }
    }

    /**
     * Gets class name for a migration status.
     * @param {string} status device status (not detailed)
     * @return {string} class name
     */
    getStatusClass(status: string): string {
        let className = status;
        if (status.includes("error")) {
            className = "error";
        }
        else if (status.includes("changing")) {
            className = "changing";
        }
        return className;
    }

    /**
     * Gets icon name for a migration status.
     * @param {string} status device status (not detailed)
     * @return {string} Material icon name
     */
    getStatusIcon(status: string): string {
        return this.iconsForStatus[this.getStatusClass(status)];
    }

    showAppMenu(): void {
        this.appMenu.show();
    }

    /**
     * Changes the application mode to the given one.
     * @param {string} newMode one of "showingDevices", "addingQr", "addingCode", "showingReports"
     */
    changeMode(newMode: string): void {
        this.mode = newMode;
    }

    /**
     * Adds a new device of the given code to the list.
     * @param {string} code device numerical code
     */
    addDevice(code: string): void {
        this.devices.push({
            code: code, status: "initial", statusDetailed: "initial", targetUrl: "", originalUrls: []
        });
        this.devicesChanged();
    }

    /**
     * Stores devices list into the local storage when a device changes.
     */
    devicesChanged(): void {
        // do not store detailed status, it is not useful for restoring
        const devicesToStore = [];
        let tmpAnyDeviceInProgress = false;
        for (const device of this.devices) {
            if (device.status.startsWith("changing")) {
                tmpAnyDeviceInProgress = true;
            }
            devicesToStore.push({
                code: device.code, originalUrls: device.originalUrls, targetUrl: device.targetUrl, status: device.status
            });
        }
        window.localStorage.setItem("deviceMigrationDevices", JSON.stringify(devicesToStore));
        this.anyDeviceInProgress = tmpAnyDeviceInProgress;
    }

    deleteAllClicked(): void {
        this.deleteAllDialog.open = true;
    }

    deleteAllDialogClosed(e): void {
        if (e.detail.action === "delete") {
            this.devices.splice(0, this.devices.length);
            this.devicesChanged();
        }
    }

    deleteDeviceClicked(device): void {
        this.deleteDeviceDialog.open = true;
        this.selectedDevice = device;
    }

    deleteDeviceDialogClosed(e): void {
        if (e.detail.action === "delete") {
            this.devices.splice(this.devices.indexOf(this.selectedDevice), 1);
            this.devicesChanged();
        }
    }

    retryFailedClicked(): void {
        this.devices.forEach(function(device): void {
            this.retryDevice(device);
        }, this);
    }

    /**
     * Changes parameters in the device and/or in the infosystem, according
     * to the current status of the device.
     * @param {Device} device device to be changed
     */
    retryDevice(device): void {
        if (device.status === "error_changing_device") {
            this.changeDevice(device);
        }
        else if (device.status === "error_changing_infosystem") {
            this.changeInfosystem(device);
        }
    }

    doFetch(url, method = "GET", body: object = null): Promise<void> {
        const httpClient = new HttpClient();
        let bodyString = null;
        if (body) {
            bodyString = JSON.stringify(body);
        }
        return httpClient.fetch(this.webApiUrl + url, {
            method: method,
            body: bodyString
        })
        .then(function(response) {
            if (response.ok) {
                return response.json();
            }
            else {
                const errorResponse = response.text();
                return errorResponse.then(function(error) {
                    return Promise.reject(new Error(error));
                });
            }
        })
        .catch(function(error) {
            return Promise.reject(new Error(error));
        });
    }

    /**
     * Shows the dialog with available target servers.
     */
    openMigrateDialog(): void {
        this.migrateDialog.open = true;

        if (this.targetServers.length > 0 && !this.selectedTargetServerUrl) {
            this.selectTargetElem.select(0);
        }
    }

    migrateDialogClosed(e): void {
        if (e.detail && e.detail.action === "migrate") {
            this.migrate();
        }
    }

    /**
     * For all devices, changes the device parameters and infosystem to contain the selected target server URL.
     */
    migrate(): void {
        this.devices.forEach(function(device) {
            device.targetUrl = this.selectedTargetServerUrl;
        }, this);
        this.devicesChanged();

        this.devices.forEach(function(device) {
            this.changeDevice(device);
        }, this);
    }

    /**
     * Gets status of changing device from backend and starts change in infosystem when it is finished.
     */
    checkMigrationStatus(device): void {
        this.doFetch("devices/" + device.code + "/status")
        .then(function(status): void {
            if (status.status !== device.statusDetailed) {
                device.statusDetailed = status.status;
            }
            // if changing device fails, it is indicated only by its status
            // (because POST devices/:deviceCode is finished before the failure and does not return the error)
            if (status.status.includes("error")) {
                throw new Error("Changing parameters in device failed");
            }
            // can be called also when infosystem change is in progress
            if (["changing_device_done", "done"].includes(status.status)) {
                device.statusDetailed = status.status;
                if (status.status === "changing_device_done") {
                    this.changeInfosystem(device);
                }
                else if (status.status === "done") {
                    device.status = status.status;
                    this.devicesChanged();
                }
            }
            else {
                setTimeout(this.checkMigrationStatus.bind(this, device), 5e2);
            }
        }.bind(this))
        .catch(function(error): void {
            console.error(error);
            device.status = device.statusDetailed = "error_changing_device";
            this.devicesChanged();
        }.bind(this));
    }

    /**
     * Changes URL of a given device in the device itself.
     * If the change is successful, change in information system will follow automatically.
     * @param {Device} device device with a new value of URL in the property targetUrl
     */
    changeDevice(device): void {
        this.doFetch("devices/" + device.code + "/servers")
        .then(function(urls): void {
            device.originalUrls = urls;
            this.devicesChanged();
        }.bind(this))
        .then(function(): void {
            return this.doFetch("devices/" + device.code, "POST", { url: device.targetUrl });
        }.bind(this))
        .then(function(): void {
            device.status = "changing_device";
            device.statusDetailed = "changing_device_waiting";
            this.devicesChanged();
            this.checkMigrationStatus(device);
        }.bind(this))
        .catch(function(error): void {
            console.error(error);
            device.status = device.statusDetailed = "error_changing_device";
            this.devicesChanged();
        }.bind(this));
    }

    /**
     * Changes URL of a given device in the infosystem.
     * @param {Device} device device with a new value of URL in the property targetUrl
     */
    changeInfosystem(device): void {
        device.status = device.statusDetailed = "changing_infosystem";
        this.doFetch("infosystem/" + device.code, "PUT", { originalUrls: device.originalUrls, newUrl: device.targetUrl })
        .then(function(): void {
            device.status = device.statusDetailed = "done";
            this.devicesChanged();
        }.bind(this))
        .catch(function(error): void {
            console.error(error);
            device.status = device.statusDetailed = "error_changing_infosystem";
            this.devicesChanged();
        }.bind(this));
    }
}
