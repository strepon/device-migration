import {I18N} from "aurelia-i18n";
import {inject} from "aurelia-framework";
import {bindable} from "aurelia-framework";
import jsQR from "jsqr";
import "@material/mwc-fab";
import "@material/mwc-textfield";

@inject(I18N)
export class QrCodeInputCustomElement {
    code = "";
    @bindable scannedCode = "";
    manualDeviceCode = "";
    @bindable mode = "";
    i18n: I18N;
    canvasElement: any;
    video: any;
    videoPaused = false;
    videoUnsupported = true;

    constructor(i18n: I18N) {
        this.i18n = i18n;
        this.i18n.setLocale("cs");
    }

    modeChanged(newMode: string, oldMode: string): void {
        if (newMode === "addingQr") {
            this.enterAddingQr();
        }
        if (oldMode === "addingQr") {
            this.exitAddingQr();
        }
    }

    changeMode(newMode: string): void {
        this.mode = newMode;
    }

    enterAddingQr(): void {
        this.video = document.createElement("video");
        const canvas = this.canvasElement.getContext("2d");

        function drawLine(begin, end): void {
            canvas.beginPath();
            canvas.moveTo(begin.x, begin.y);
            canvas.lineTo(end.x, end.y);
            canvas.lineWidth = 12;
            canvas.strokeStyle = "#b00020";
            canvas.stroke();
        }

        function tick(): void {
            if (this.video.readyState === this.video.HAVE_ENOUGH_DATA) {
                this.canvasElement.height = this.video.videoHeight;
                this.canvasElement.width = this.video.videoWidth;
                canvas.drawImage(this.video, 0, 0, this.canvasElement.width, this.canvasElement.height);
                const imageData = canvas.getImageData(0, 0, this.canvasElement.width, this.canvasElement.height);
                const scannedCode = jsQR(imageData.data, imageData.width, imageData.height, {
                    inversionAttempts: "dontInvert",
                });
                if (scannedCode) {
                    drawLine(scannedCode.location.topLeftCorner, scannedCode.location.topRightCorner);
                    drawLine(scannedCode.location.topRightCorner, scannedCode.location.bottomRightCorner);
                    drawLine(scannedCode.location.bottomRightCorner, scannedCode.location.bottomLeftCorner);
                    drawLine(scannedCode.location.bottomLeftCorner, scannedCode.location.topLeftCorner);

                    this.scannedCode = scannedCode.data;

                    // pause video to prevent immediate repeated additions of the same code
                    this.videoPaused = true;
                    setTimeout(function(): void {
                        this.videoPaused = false;
                        requestAnimationFrame(tick.bind(this));
                    }.bind(this), 1e3);
                }
            }
            if (this.mode === "addingQr" && !this.videoPaused) {
                requestAnimationFrame(tick.bind(this));
            }
        }

        if (navigator && navigator.mediaDevices) {
            this.videoUnsupported = false;
            navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } })
            .then(function(stream): void {
                this.video.srcObject = stream;
                this.video.play();
                requestAnimationFrame(tick.bind(this));
            }.bind(this));
        }
        else {
            this.videoUnsupported = true;
        }
    }

    exitAddingQr(): void {
        if (this.video.srcObject) {
            this.video.srcObject.getTracks().forEach(function(track) {
                track.stop();
            });
        }
    }

    exitAddingCode(cancelled: boolean): void {
        if (!cancelled) {
            this.scannedCode = this.manualDeviceCode;
        }
        this.manualDeviceCode = "";
        this.changeMode("addingQr");
    }
}
