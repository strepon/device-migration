import {Aurelia} from 'aurelia-framework';
import * as environment from '../config/environment.json';
import {PLATFORM} from 'aurelia-pal';
import {I18N} from "aurelia-i18n";
import XHR from "i18next-xhr-backend";

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName("resources/index"))
    .plugin("aurelia-i18n", (instance) => {
        // register backend plugin
        instance.i18next.use(XHR);
        return instance.setup({
            backend: {
                loadPath: "/locale/{{lng}}/{{ns}}.json"
            },
            lng: "cs",
            attributes: ["t", "i18n"],
            fallbackLng: "en",
            debug: false,
            ns: ["translation"]
        });
    });

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

    if (environment.testing) {
        aurelia.use.plugin(PLATFORM.moduleName("aurelia-testing"));
        aurelia.use.plugin(PLATFORM.moduleName("aurelia-i18n"));
    }

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('device-migration')));
}
