const Koa = require("koa");
const router = require("koa-router")();
const cors = require("@koa/cors");
const bodyParser = require("koa-bodyparser");

const app = new Koa();
app.use(bodyParser());

const portNumber = process.argv[2] || 8082;
app.listen(portNumber);

const servers = [
    { name: "Vakojaguár", url: "vakojaguar.example.com" },
    { name: "Claudiosaurus", url: "clsaurus.example.com" },
    { name: "Vydra", url: "vydra.example.org" },
    { name: "Serikornis", url: "serikornis.example.org" },
    { name: "Liška", url: "liska.example.org" }
];

const checkError = function(error, ctx) {
    if (error) {
        ctx.status = 500;
        ctx.body = error;
        ctx.app.emit("error", new Error(error), ctx);
    }
}

// fake in-memory storage of devices to keep their
// properties consistent during one backend session
const devices = {};

/**
 * @swagger
 * /api/targetservers
 *   get:
 *     description: Returns the list of available servers.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: List of servers as objects with name and URL
 *           schema:
 *             properties:
 *               name:
 *                 type: string
 *               url:
 *                 type: string
 */
router.get("/targetservers", async(ctx) => {
    // use only a random subset of servers
    const serversAvailable = servers.slice(0);
    for (let s = serversAvailable.length - 1; s >= 0; s--) {
        if (Math.random() > 0.7) {
            serversAvailable.splice(s, 1);
        }
    }
    ctx.body = serversAvailable;
});

/**
 * @swagger
 * /api/devices/{deviceCode}/servers
 *   get:
 *     description: Returns the list of servers where a device is registered.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: List of server URLs
 */
router.get("/devices/:deviceCode/servers", async(ctx) => {
    const code = ctx.params.deviceCode;

    if (code in devices) {
        ctx.body = devices[code].urls;
    }
    else {
        // initial fake determination of server by code length
        // also from the servers that are not available
        const serverUrls = [];
        serverUrls.push(servers[code.length % (servers.length)].url);
        if (code.length > 5) {
            const anotherServerUrl = servers[code.length % 3].url;
            if (serverUrls[0] !== anotherServerUrl) {
                serverUrls.push(anotherServerUrl);
            }
        }
        devices[code] = {};
        devices[code].urls = serverUrls;
        ctx.body = serverUrls;
    }
});

/**
 * @swagger
 * /api/devices/{deviceCode}/status
 *   get:
 *     description: Returns status of a given device which is one of "changing_device_waiting",
 *       "changing_device_processing", "changing_device_done", "changing_infosystem_processing"
 *       and "changing_infosystem_done".
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Device status
 *           schema:
 *             properties:
 *               status:
 *                 type: string
 */
router.get("/devices/:deviceCode/status", async(ctx, next) => {
    const code = ctx.params.deviceCode;

    let error = "";
    if (code in devices) {
        ctx.body = { status: devices[code].status };
    }
    else {
        error = "Invalid device code " + code;
    }

    checkError(error, ctx);
    return next();
});

/**
 * @swagger
 * /api/devices/{deviceCode}
 *   post:
 *     description: Starts process of updating parameters of a given device.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: deviceParameters
 *         description: New parameters for the device
 *         in: body
 *         required: true
 *         schema:
 *           code:
 *             type: string
 *           url:
 *             type: string
 *     responses:
 *       200:
 *         description: Updating successfully started
 */
router.post("/devices/:deviceCode", async(ctx, next) => {
    const code = ctx.params.deviceCode;
    const urlToDevice = ctx.request.body.url;

    let error = "";
    if (code in devices) {
        const parametersToChange = {
            code: code,
            url: urlToDevice,
            ctx: ctx
        };

        // simulate processing parameter change in the device by random timeouts
        const moveStatusForward = function() {
            let error = "";
            if (this.code in devices) {
                // simulate failure of device change
                if (Math.random() > 0.95) {
                    error = "Changing parameters in device failed";
                }

                if (devices[this.code].status === "changing_device_waiting") {
                    devices[this.code].status = "changing_device_processing"; // parameters are being processed in the device
                    setTimeout(moveStatusForward.bind(this), 1e4 * Math.random());
                }
                else if (devices[this.code].status === "changing_device_processing") {
                    devices[this.code].url = this.url;
                    devices[this.code].status = "changing_device_done"; // parameters change in the device has been done
                }
                else {
                    error = "Invalid status " + devices[this.code].status;
                }

                if (error) {
                    devices[this.code].status = "error_changing_device";
                }
            }
        };

        devices[code].status = "changing_device_waiting"; // new parameters waiting for the device
        setTimeout(moveStatusForward.bind(parametersToChange), 1e4 * Math.random());
        ctx.body = true;
    }
    else {
        error = "Invalid device code " + code;
    }
    checkError(error, ctx);
    return next();
});

/**
 * @swagger
 * /api/infosystem/{deviceCode}
 *   post:
 *     description: Updates device URL in information system.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: deviceUrls
 *         description: Original URLs and new URL for the device
 *         in: body
 *         required: true
 *         schema:
 *           originalUrls:
 *             type: Array
 *           newUrl:
 *             type: string
 *     responses:
 *       200:
 *         description: Information system successfully updated
 */
router.put("/infosystem/:deviceCode", async(ctx, next) => {
    const code = ctx.params.deviceCode;
    const originalUrls = ctx.request.body.originalUrls;
    const newUrl = ctx.request.body.newUrl;

    let error = "";
    if (code in devices) {
        // simulate failure of infosystem change
        if (Math.random() > 0.95) {
            error = "Changing parameters in infosystem failed";
        }
        devices[code].status = "changing_infosystem";
        // simulate processing in the information system
        await new Promise(function(resolve) {
            setTimeout(resolve, 1e4 * Math.random());
        });
        devices[code].urls.push(newUrl);
        devices[code].status = "done";
        ctx.body = true;
    }
    else {
        error = "Invalid device code " + code;
    }
    checkError(error, ctx);
    return next();
});

function checkOrigin(ctx) {
    const requestOrigin = ctx.accept.headers.origin;
    if (["http://localhost:8080", "http://46.28.108.168"].includes(requestOrigin)) {
        return requestOrigin;
    }
    else {
        return ctx.throw("Not valid origin.");
    }
}

app.use(cors({ origin: checkOrigin }));
app.use(router.routes());
