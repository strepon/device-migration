# Device migration

This application is used for creating a list of devices (by scanning or
manual entering their codes) and migrating of these devices to a different server.
The list with the status of the migration can be exported as a report.

The (pseudo)backend simulates progress of the migration, including random failures
during different steps of the process.

The application is available online at: http://46.28.108.168/html/

## How to build frontend

Run `npm install`, `npm run build` (or `au build --env prod`) and use the result from the `dist` directory.

## How to run frontend locally

Run `npm start` (or `au run`). Then the application can be accessed at `http://localhost:8080`.

## How to run backend

For backend, use the files in the `webapi` folder, run `npm install` and `node device-migration-server.js`.

The backend runs on the port `8082` by default. The settings of this port is hardcoded on the frontend side.
